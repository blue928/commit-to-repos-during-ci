# Commit To Self and To Project Wiki and External Repos During Pipeline

## If This Helps You, Please Star This Project :)

One click can help us keep providing and improving Guided Explorations.  If you find this information helpful, please click the star on this project's details page! [Project Details](https://gitlab.com/guided-explorations/gitlab-ci-yml-tips-tricks-and-hacks/commit-to-repos-during-ci/commit-to-repos-during-ci)

## Demonstrates These Design Requirements, Desirements and AntiPatterns

* **DevOps Development Pattern [![FC](https://gitlab.com/guided-explorations/guided-exploration-concept/-/raw/master/images/FC.png)](https://about.gitlab.com/features/):** Retrieve external repositories into a pipeline.
* **DevOps Development Common Pattern [![FC](https://gitlab.com/guided-explorations/guided-exploration-concept/-/raw/master/images/FC.png)](https://about.gitlab.com/features/):** Commit to repositories during CI. In general it is good to avoid commiting to respositories during CI.  However, it is also the only way that the Git repository can be the Single Source of Truth for state data that is necessarily updated during CI runs. Storing this data as CI variables is a way to avoid committing to the repository and that is demonstrated in [Write CI-CD Variables in Pipeline For Generic Package Version Management](https://gitlab.com/guided-explorations/cfg-data/write-ci-cd-variables-in-pipeline)
* **DevOps Development Pattern [![FC](https://gitlab.com/guided-explorations/guided-exploration-concept/-/raw/master/images/FC.png)](https://about.gitlab.com/features/):** Adding release notes or other release information to files such as CHANGELOG.md or, in the case of GitLab, in the "wiki" of the project.
* **DevOps Development Pattern [![FC](https://gitlab.com/guided-explorations/guided-exploration-concept/-/raw/master/images/FC.png)](https://about.gitlab.com/features/):** Adding tags to commits as a part of successful CI operations such as packaging.
* **GitLab Development Pattern [![FC](https://gitlab.com/guided-explorations/guided-exploration-concept/-/raw/master/images/FC.png)](https://about.gitlab.com/features/):** Commiting changes to the Wiki of a GitLab Project - which is just another Git repo under the covers.

### Uses and features for this example:
* Across Pipeline Version data persistence in files (or other data)
* Automated release notes and/or CHANGELOG.md
* Automated Project Wiki Update (GitLab project wikis are implemented as standalone Git repos of markdown files)

## Guided Explorations Concept

This Guided Exploration is built according to a specific vision and requirements that maximize its value to both GitLab and GitLab's customers.  You can read more here: [The Guided Explorations Concept](https://gitlab.com/guided-explorations/guided-exploration-concept/blob/master/README.md)

## Working Design Pattern

As originally built, this design pattern works and can be tested. In the case of plugin extensions like this one, the working pattern may be it's use in another Guided Exploration.

## Overview Information

GitLab's features are constantly and rapidly evolving and we cannot keep every example up to date.  The date and version information are published here so that you can assess if new features mean that the example could be enhanced or does not account for an new capability of GitLab.

* **Product Manager For This Guided Exploration**: Darwin Sanoy (@DarwinJS)

* **Publish Date**: 2020-11-19

* **GitLab Version Released On**: 13.5

* **GitLab Edition Required**: 

  * For overall solution: [![FC](https://gitlab.com/guided-explorations/guided-exploration-concept/-/raw/master/images/FC.png)](https://about.gitlab.com/features/) 

    [Click to see Features by Edition](https://about.gitlab.com/features/) 

* **Tested On**: 
  * GitLab Docker-Executor Runner using bash:latest container

* **References and Featured In**:
  * 
